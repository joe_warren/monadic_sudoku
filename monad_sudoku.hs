import System.Environment (getArgs)
import Control.Monad
import qualified Data.Map.Strict as Map
import Data.List
import Data.Maybe (fromMaybe)
import Text.Read (readMaybe)

data Index = One | Two | Three | Four | Five | Six | Seven | Eight | Nine deriving (Eq, Ord, Enum, Bounded, Show) 

squares = [[One, Two, Three], [Four, Five , Six], [Seven, Eight, Nine]]

generateEnumValues :: (Enum a) => [a]
generateEnumValues = enumFrom (toEnum 0)

indices :: [Index]
indices = generateEnumValues

type Point = (Index, Index)

points = [(x, y) | y<-indices , x<-indices]

row :: Point -> [Point]
row p = map (\i -> (i, snd p)) indices
column :: Point -> [Point]
column p = map (\i -> (fst p, i)) indices
square :: Point -> [Point]
squareMap = Map.fromList $ map (\x -> (x, head $ filter (elem x) squares)) indices

square (i, j) = liftM2 (,) (squareLookup i) (squareLookup j)
    where squareLookup i = fromMaybe [] $ Map.lookup i squareMap

type Sudoku = Map.Map Point Index 

splitOn :: Char -> String -> [String]
splitOn delim [] = [""] 
splitOn delim (c:cs) 
   | c == delim = "" : rest
   | otherwise = (c : head rest) : tail rest
   where
       rest = splitOn delim cs 

safeToEnum :: (Enum t, Bounded t) => Int -> Maybe t
safeToEnum i = let r = toEnum i
                   max = maxBound `asTypeOf` r
                   min = minBound `asTypeOf` r
               in if i >= fromEnum min && i <= fromEnum max
               then Just r
               else Nothing

parsePoint :: String -> Either String (Maybe Index)
parsePoint "*" = Right Nothing
parsePoint s =
    case readMaybe s :: Maybe Int of
        Just a -> case safeToEnum (a-1) :: Maybe Index of
                    Just i -> Right $ Just i
                    Nothing -> Left("Value out of bounds: " ++ s)
        Nothing -> Left("Invalid value: " ++ s)

flattenSudoku :: [(Point, Maybe Index)] -> [(Point, Index)]
flattenSudoku a = a >>= \x -> flatten x
    where 
        flatten (p, Just i) = [(p, i)]
        flatten (p, Nothing) = []

parseSudoku :: String -> Either String Sudoku
parseSudoku str = do
    let strs = filter (""/=) $ splitOn ' ' str
    if (length points /= length strs) then
        Left ("Sudoku string had the wrong length " ++ (show $ length strs) ++ 
            " not " ++ (show $ length points))
    else 
        return ()
    
    vals <- mapM parsePoint strs 
    let fullMap = zip points vals
    return $ Map.fromList $ flattenSudoku fullMap

emplace :: Point -> Index -> Sudoku -> Sudoku
emplace = Map.insert

propogatePoint :: Point -> Sudoku -> Sudoku
propogatePoint p s = case Map.lookup p s of
                        Just _ -> s
                        Nothing -> case viable of
                                    (v:[]) -> emplace p v s
                                    _ -> s
    where 
        collisions = do
            shared <- row p ++ column p ++ square p
            case Map.lookup shared s of
                Just j -> [j]
                Nothing -> []
        viable = filter (not.(flip elem) collisions) indices 

propogate :: Sudoku -> Sudoku
propogate s = foldr propogatePoint s points

solveStep :: Sudoku -> Point -> [Sudoku]
solveStep s p = do
    x <- case Map.lookup p s of
            Just i -> return s
            Nothing -> do 
                let collisions = do
                    shared <- row p ++ column p ++ square p
                    case Map.lookup shared s of
                        Just j -> [j]
                        Nothing -> []
                viable <- indices \\ collisions
                return $ propogate $ emplace p viable s 
    return x

solveSudoku :: Sudoku -> [Sudoku]
solveSudoku sudoku = foldM solveStep (propogate sudoku) points 

showPoint :: Point -> Sudoku -> String 
showPoint p s = case Map.lookup p s of
                    Just i -> show $ 1 + fromEnum i
                    Nothing -> "*"
showSudoku :: Sudoku -> String 
showSudoku s = unlines [unwords [showPoint (i,j) s | i<-indices] | j<-indices]

main = do
    args <- getArgs
    forM args $ \a -> case parseSudoku a of
                        Right sudoku -> putStrLn $ unlines $ map showSudoku $ solveSudoku sudoku
                        Left error -> putStrLn error
